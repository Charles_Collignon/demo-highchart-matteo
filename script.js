let easeInOut = function (t) {
  return t * (2 - t);
};

Math.easeInOut = easeInOut;

// China
const quantChina = "./json/china_q3_data.json";
const offChina = "./json/china_off_data.json";
const outputQuantChina = [];
const outputOffChina = [];
// USA
const quantUSA = "./json/usa.json";
const offUSA = "./json/usa_off_data.json";
const outputQuantUSA = [];
const outputOffUSA = [];
// EUROZONE
const euroJSON = "./json/eurozone.json";
const outputEuro = [];
const outputGDPUSA = [];
const outputGDPCHINA = [];

const outputs = [
  outputQuantChina,
  outputQuantUSA,
  outputEuro,
  outputOffUSA,
  outputOffChina,
  outputGDPUSA,
  outputGDPCHINA,
];

Promise.all([
  fetch(quantChina),
  fetch(offChina),
  fetch(quantUSA),
  fetch(offUSA),
  fetch(euroJSON),
])
  .then((responses) => {
    return Promise.all(
      responses.map((response) => {
        return response.json();
      })
    );
  })
  .then((data) => {
    const dateChina = data[0];
    const dateOffChina = data[1];
    const dateUSA = data[2];
    const dateOffUSA = data[3];
    const dateEuro = data[4];

    let china = dateChina.Q3_GDP_CHINA;
    let offDataChina = dateOffChina.Official_GDP_CHINA;
    let usa = dateUSA.United_States;
    let offDataUSA = dateOffUSA.Official_GDP_US;
    let euro = dateEuro.Eurozone;

    for (const property in china) {
      let datum = Date.parse(property);
      let item = [datum, china[property]];
      outputQuantChina.push(item);
    }
    for (const property in usa) {
      let datum = Date.parse(property);
      let item = [datum, usa[property]];
      outputQuantUSA.push(item);
    }
    for (const property in euro) {
      let datum = Date.parse(property);
      let item = [datum, euro[property]];
      outputEuro.push(item);
    }
    for (const property in offDataUSA) {
      let datum = Date.parse(property);
      let item = [datum, offDataUSA[property]];
      outputOffUSA.push(item);
    }
    for (const property in offDataChina) {
      let datum = Date.parse(property);
      let item = [datum, offDataChina[property]];
      outputOffChina.push(item);
    }
    return outputs;
  })
  .then((outputs) => {
    Highcharts.stockChart("container", {
      tooltip: {
        xDateFormat: "%b %d, %Y",
        shared: true,
        split: true,
        crosshairs: true,
      },

      xAxis: {
        labels: {
          enabled: true,
        },
        gridLineWidth: 1,
      },

      yAxis: {
        gridLineWidth: "0.5px",
        showLastLabel: true,
      },

      title: {
        text: "QuantCube Economic Growth Index",
        style: {
          color: "#457B9D",
          fontSize: 20,
        },
      },

      plotOptions: {
        series: {
          showInNavigator: true,
          dataLabels: {
            enabled: false,
          },
          states: {
            hover: {
              lineWidthPlus: -1,
            },
          },
        },
      },

      credits: {
        enabled: false,
      },

      legend: {
        enabled: true,
        layout: "horizontal",
        align: "center",
        verticalAlign: "bottom",
        borderWidth: 0,
        itemStyle: {
          fontSize: "20px",
        },
      },

      rangeSelector: {
        buttons: [
          {
            type: "month",
            count: 1,
            text: "1M",
          },
          {
            type: "month",
            count: 3,
            text: "3M",
          },
          {
            type: "month",
            count: 6,
            text: "6M",
          },
          {
            type: "year",
            count: 1.5,
            text: "1Y",
          },
          {
            type: "all",
            text: "All",
          },
        ],
        selected: 3,
      },

      colorAxis: {
        gridLineColor: "#1D3557",
        gridLineWidth: "20px",
      },

      series: [
        // {
        //   name: "Quantcube China",

        //   data: outputQuantChina,
        //   color: "#cb0b0a",
        //   lineWidth: "5px",
        //   zIndex: 1,
        //   type: "spline",
        //   animation: {
        //     // defer: 500,
        //     duration: 8000,
        //     easing: "easeInOut",
        //   },
        // },
        // {
        //   name: "Official China GDP",

        //   data: outputOffChina,
        //   color: "#df8080",
        //   lineWidth: 1,
        //   zIndex: 6,
        //   type: "line",
        //   animation: {
        //     defer: 3000,
        //     duration: 8000,
        //     easing: "easeInOut",
        //   },
        //   marker: {
        //     enabled: true,
        //     radius: 6,
        //   },
        // },
        {
          name: "Quantcube USA",

          data: outputQuantUSA,
          color: "#457b9d",
          lineWidth: "5px",
          zIndex: 2,
          type: "spline",
          animation: {
            defer: 1000,
            duration: 7000,
            easing: "easeInOut",
          },
        },
        {
          name: "Official USA GDP",

          data: outputOffUSA,
          color: "#cb0b0a",
          lineWidth: 1,
          zIndex: 5,
          type: "line",
          animation: {
            defer: 3000,
            duration: 8000,
            easing: "easeInOut",
          },
          marker: {
            enabled: true,
            radius: 6,
          },
        },
        // {
        //   name: "Quantcube Eurozone GDP",

        //   data: outputEuro,
        //   color: "#52b788",
        //   lineWidth: "5px",
        //   zIndex: 2,
        //   type: "spline",
        //   animation: {
        //     defer: 2000,
        //     duration: 6000,
        //     easing: "easeInOut",
        //   },
        // },
      ],
    });
  });
